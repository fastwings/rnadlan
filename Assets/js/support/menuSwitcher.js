var switcherMenu = {

	newNewsElement : null,
	listNewsElement : null,
	newPageElement : null,
	listPageElement : null,
	newPageCode : 9,
	newNewsCode : 10,
	init : function(menuCode) {
		var me = this;
		switch(menuCode) {
			case me.newNewsCode:
			case me.newPageCode:
				//new page
				me._cheangeMenu('Save', 'Reset', 'Preview');
				break;
			default:
				//all Else
				me._cheangeMenu('Add', 'Edit', 'Delete');
				break;
		}
	},
	onCloseEvent : function(menuCode) {

		var me = this;
		switch(menuCode) {
			case me.newNewsCode:
				//new news
				me.newNewsElement = null;
				break;
			case me.newPageCode:
				//new page
				me.newPageElement = null;
				break;
			default:
				//all Else
				break;
		}
	},
	onClickEvent : function(menuCode, eventType) {
		var me = this;
		switch(menuCode) {

			case me.newNewsCode:
				//new news
				$("#ElementsEvent").attr('type', eventType);
				$("#ElementsEvent").trigger(eventNames.handleNews);
				break;
			case me.newPageCode:
				//new page
				$("#ElementsEvent").attr('type', eventType);
				$("#ElementsEvent").trigger(eventNames.handlePage);
				break;
			default:
				//all Else
				break;
		}
	},
	_cheangeMenu : function(addBtnTxt, editBtnTxt, deleteBtnTxt) {
		try {
			var addBtnElemnt = Ext.getCmp('ToolBarControlAdd');
			var editBtnElemnt = Ext.getCmp('ToolBarControlEdit');
			var deleteBtnElemnt = Ext.getCmp('ToolBarControlDel');
			addBtnElemnt.setText(addBtnTxt);
			editBtnElemnt.setText(editBtnTxt);
			deleteBtnElemnt.setText(deleteBtnTxt);
		} catch(e) {
		}
	}
}