//event trigger names
var eventNames = {
	//page section area
	newPage: "newPage",
	handlePage: "handlePage",
	listPage: "listPages",
	//news section area
	newNews: "newNews",
	handleNews: "handleNews",
	listNews: "listNews",
	//user+misc section 
	userLogout: "logout"
}
