Ext.define('MyApp.view.ui.Windows.NewsList', {
	extend : 'Ext.grid.Panel',
	xtype : 'gridpanel',
	autoScroll : true,
	id : 'NewsList',
	autoheight : true,
	param : 9,
	title : 'News List',
	autoShow : true,
	draggable : false,
	closable : true,
	style : {
		overflow : 'scroll'
	},
	title : 'News List Mangment',
	store : data,
	bbar : Ext.create('Ext.PagingToolbar', {
		store : data,
		displayInfo : true,
		displayMsg : 'Displaying topics {0} - {1} of {2}',
		emptyMsg : "No topics to display",
	}),
	viewConfig : {

	},
	columns : [{
		id : 'company',
		text : 'Company',
		sortable : true,
		dataIndex : 'company',
		flex : 1
	}, {
		text : 'Price',
		sortable : true,
		renderer : Ext.util.Format.usMoney,
		dataIndex : 'price',
		width : 75
	}, {
		text : 'Change',
		sortable : true,
		renderer : change,
		dataIndex : 'change',
		width : 75
	}, {
		text : '% Change',
		sortable : true,
		renderer : pctChange,
		dataIndex : 'pctChange',
		width : 75
	}, {
		text : 'Last Updated',
		sortable : true,
		dataIndex : 'lastChange',
		width : 75
	}],
	initComponent : function() {
		var me = this;

		Ext.applyIf(me, {
			dockedItems : []
		});
		switcherMenu.init(0);
		me.callParent(arguments);
	}
});
