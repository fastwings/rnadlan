Ext.define('MyApp.view.ui.Windows.PagePanle', {
	extend : 'Ext.form.FormPanel',
	xtype : 'panel',
	autoScroll : true,
	requires: [''],
	id : 'PageForm',
	autoheight : true,
	param : 9,
	title : 'Page Form',
	autoShow : true,
	draggable : false,
	closable : true,
	style : {
		overflow : 'scroll'
	},

	initComponent : function() {
		var me = this;

		Ext.applyIf(me, {
			dockedItems : [{
				xtype : 'fieldset',
				collapsible : false,
				title : 'Meta',
				items : [{
					id : 'page_meta_key',
					xtype : 'textarea',
					allowBlank : false,
					anchor : '100%',
					fieldLabel : 'Meta Key'
				}, {
					id : 'page_meta_desc',
					xtype : 'textarea',
					allowBlank : false,
					anchor : '100%',
					fieldLabel : 'Meta Description'
				}, {
					id : 'page_meta_title',
					xtype : 'textfield',
					anchor : '100%',
					allowBlank : false,
					fieldLabel : 'Title'
				}]
			}, {
				xtype : 'hidden',
				name : 'page_id',
				id : 'page_id',
				value : '-1'
			}, {
				xtype : 'fieldset',
				title : 'Page Data',
				collapsible : false,
				items : [{
					id : 'page_desc',
					xtype : 'textarea',
					anchor : '100%',
					fieldLabel : 'Page Description'
				}, {
					id : 'page_subtitle',
					xtype : 'htmleditor',
					anchor : '100%',
					fieldLabel : 'Subtitle'
				}, {
					id : 'page_body',
					xtype : 'tinymcefield',
					height : '600',
					style : 'background-color: white;',
					fieldLabel : 'Body',
					anchor : '100%',
					tinymceConfig : {
						accessibility_focus : false,
						language : "en",
						mode : "none",
						skin : "o2k7",
						theme : "advanced",

						plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,extjsfilemanager",
						theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
						theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
						theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
						theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,|,extjsfilemanagerselectimage,extjsfilemanagerselectmedia,extjsfilemanagerupload",

						extjsfilemanager_handlerurl : 'http://localhost/Davidprojects/rnadlan/BrowserHandler.php',
						extjsfilemanager_extraparams : {
							param1 : 'value1',
							param2 : 'value2'
						}, // extended_valid_elements:// extended_valid_elements:
						// "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
						// template_external_list_url:
						// "example_template_list.js"
						// content_css : "/lib/css/editor.css",
						theme_advanced_toolbar_location : 'top',
						theme_advanced_toolbar_align : 'left',
						theme_advanced_statusbar_location : 'bottom',
						theme_advanced_resize_horizontal : false,
						theme_advanced_resizing : true,
						width : '100%',
						height : '600'
					}
				}]
			}]
		});
		switcherMenu.init(switcherMenu.newPageCode);
		me.callParent(arguments);
	}
});
