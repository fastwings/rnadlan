Ext.define('MyApp.view.ui.Windows.NewsPanle', {
	extend : 'Ext.form.FormPanel',
	xtype : 'panel',
	autoScroll : true,
	id : 'NewsForm',
	autoheight : true,
	param : 10,
	title : 'Page Form',
	autoShow : true,
	draggable : false,
	closable : true,
	style : {
		overflow : 'scroll'
	},

	initComponent : function() {
		var me = this;

		Ext.applyIf(me, {
			dockedItems : [{
				xtype : 'fieldset',
				collapsible : false,
				title : 'News',
				items : [{
					id : 'news_title',
					xtype : 'textfield',
					allowBlank : false,
					anchor : '100%',
					fieldLabel : 'News Title'
				}, {
					id : 'news_text',
					xtype : 'htmleditor',
					anchor : '100%',
					fieldLabel : 'News Body'
				}, {
					id : 'news_youtube',
					xtype : 'textfield',
					anchor : '100%',
					fieldLabel : 'News YoutubeLink'
				}]
			}, {
				xtype : 'hidden',
				name : 'news_id',
				id : 'news_id',
				value : '-1'
			}]
		});
		switcherMenu.init(switcherMenu.newNewsCode);
		me.callParent(arguments);
	}
});
