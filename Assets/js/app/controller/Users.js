/**
 *
 */
Ext.define('MyApp.controller.Users', {
	extend : 'Ext.app.Controller',
	init : function() {
		this.wireEvetns();
	},
	wireEvetns : function() {
		var me = this;
		$("#ElementsEvent").live(eventNames.userLogout, function() {
			me.dologout();
		});
	},
	dologout : function() {
		Ext.MessageBox.confirm('Confirm', 'Are you sure you want to do that?', showResutl);
		function showResutl(btn) {
			if(btn == 'yes') {
				$.ajax({
					url : requestConfig.DoLogout,
					success : function(result) {
						window.location = siteUrl;
					}
				});
			}
		}

	}
});
