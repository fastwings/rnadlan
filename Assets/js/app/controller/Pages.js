Ext.define('MyApp.controller.Pages', {
	extend : 'Ext.app.Controller',

	init : function() {
		this.wireEvetns();
	},
	wireEvetns : function() {
		var me = this;
		$("#ElementsEvent").live(eventNames.listPage, function(event, data) {
			var tabPanel = Ext.getCmp('TabPableContiner');
			if(switcherMenu.listPageElement != null) {
				var ementCheck = Ext.getCmp(switcherMenu.listPageElement.id);
				if(ementCheck != undefined)
					tabPanel.setActiveTab(switcherMenu.listPageElement);
				else {
					switcherMenu.listPageElement = Ext.create('MyApp.view.ui.Windows.PageList');
					tabPanel.add(switcherMenu.listPageElement).show();
				}
			} else {

				switcherMenu.listPageElement = Ext.create('MyApp.view.ui.Windows.PageList');
				tabPanel.add(switcherMenu.listPageElement).show();
			}
		});
		$("#ElementsEvent").live(eventNames.newPage, function() {
			var tabPanel = Ext.getCmp('TabPableContiner');
			if(switcherMenu.newPageElement != null) {
				var ementCheck = Ext.getCmp(switcherMenu.newPageElement.id);
				if(ementCheck != undefined)
					tabPanel.setActiveTab(switcherMenu.newPageElement);
				else {
					switcherMenu.newPageElement = Ext.create('MyApp.view.ui.Windows.PagePanle');
					tabPanel.add(switcherMenu.newPageElement).show();
				}
			} else {

				switcherMenu.newPageElement = Ext.create('MyApp.view.ui.Windows.PagePanle');
				tabPanel.add(switcherMenu.newPageElement).show();
			}
		});
		$("#ElementsEvent").live(eventNames.handlePage, function(event, data) {
			var typeAction = $("#ElementsEvent").attr('type');
			var formObj = {};
			var canSubmit = false;
			if(typeAction == "add" || typeAction == "edit") {
				var formPanel = Ext.getCmp('PageForm');
				var form = formPanel.getForm();
				if(form.isValid()) {
					canSubmit = true;
				}
			}
			if(canSubmit) {
				switch (typeAction) {
					case "add":
						formObj = me._prepForm(false);
						//alert(formObj);
						$.post(requestConfig.AddPage, formObj, function(msg) {
							me._onSuccessPage(msg);
						});
						break;
				}
			} 
		});
	},
	_onSuccessPage : function(data) {
		var responseObject = data;
		if(data === "String") {
			responseObject = Ext.JSON.decode(data);
		}
		if(responseObject.success) {
			var panel = Ext.getCmp('PageForm');
			var tabPanel = Ext.getCmp('TabPableContiner');
			switcherMenu.newPageElement = null;
			switcherMenu.init(0);
			panel.close();
			tabPanel.setActiveTab(0);

		}
	},
	_prepForm : function(isEdit) {

		var eleId = Ext.getCmp('page_id');
		var eleMetaKey = Ext.getCmp('page_meta_key');
		var eleMetaDesc = Ext.getCmp('page_meta_desc');
		var eleMetaTitle = Ext.getCmp('page_meta_title');
		var eleDesc = Ext.getCmp('page_desc');
		var eleSubtitle = Ext.getCmp('page_subtitle');
		var eleBody = Ext.getCmp('page_body');
		var obj = {};
		obj.id = -1;
		obj.metaKey = eleMetaKey.getValue();
		obj.metaDesc = eleMetaDesc.getValue();
		obj.metaTitle = eleMetaTitle.getValue();
		obj.pageDesc = eleDesc.getValue();
		obj.pageSubtitle = eleSubtitle.getValue();
		obj.pageBody = eleBody.getValue();
		if(isEdit) {
			obj.id = parseInt(eleId.getValue(), 10);
		}
		return obj;
	}
});
