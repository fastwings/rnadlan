Ext.define('MyApp.controller.News', {
	extend : 'Ext.app.Controller',

	init : function() {
		this.wireEvetns();
	},
	wireEvetns : function() {
		var me = this;
		$("#ElementsEvent").live(eventNames.listNews, function(event, data) {
			var tabPanel = Ext.getCmp('TabPableContiner');
			if(switcherMenu.listNewsElement != null) {
				var ementCheck = Ext.getCmp(switcherMenu.listNewsElement.id);
				if(ementCheck != undefined)
					tabPanel.setActiveTab(switcherMenu.listNewsElement);
				else {
					switcherMenu.listNewsElement = Ext.create('MyApp.view.ui.Windows.NewsList');
					tabPanel.add(switcherMenu.listNewsElement).show();
				}
			} else {

				switcherMenu.listNewsElement = Ext.create('MyApp.view.ui.Windows.NewsList');
				tabPanel.add(switcherMenu.listNewsElement).show();
			}
		});
		$("#ElementsEvent").live(eventNames.newNews, function() {
			var tabPanel = Ext.getCmp('TabPableContiner');
			if(switcherMenu.newNewsElement != null) {
				var ementCheck = Ext.getCmp(switcherMenu.newNewsElement.id);
				if(ementCheck != undefined)
					tabPanel.setActiveTab(switcherMenu.newNewsElement);
				else {' '
					switcherMenu.newNewsElement = Ext.create('MyApp.view.ui.Windows.NewsPanle');
					tabPanel.add(switcherMenu.newNewsElement).show();
				}
			} else {

				switcherMenu.newNewsElement = Ext.create('MyApp.view.ui.Windows.NewsPanle');
				tabPanel.add(switcherMenu.newNewsElement).show();
			}
		});
		$("#ElementsEvent").live(eventNames.handleNews, function(event, data) {
			var typeAction = $("#ElementsEvent").attr('type');
			var formObj = {};
			var canSubmit = false;
			if(typeAction == "add" || typeAction == "edit") {
				var formPanel = Ext.getCmp('NewsForm');
				var form = formPanel.getForm();
				if(form.isValid()) {
					canSubmit = true;
				}
			}
			if(canSubmit) {
				switch (typeAction) {
					case "add":
						formObj = me._prepForm(false);
						//alert(formObj);
						$.post(requestConfig.AddNews, formObj, function(msg) {
							me._onSuccessNews(msg);
						});
						break;
				}
			}
		});
	},
	_onSuccessNews : function(data) {
		var responseObject = data;
		if(data === "String") {
			responseObject = Ext.JSON.decode(data);
		}

		if(responseObject.success) {
			var panel = Ext.getCmp('NewsForm');
			var tabPanel = Ext.getCmp('TabPableContiner');
			switcherMenu.newPageElement = null;
			switcherMenu.init(0);
			panel.close();
			tabPanel.setActiveTab(0);

		}
	},
	_prepForm : function(isEdit) {

		var eleId = Ext.getCmp('news_id');
		var eleTitle = Ext.getCmp('news_title');
		var eleText = Ext.getCmp('news_text');
		var eleYoutube = Ext.getCmp('news_youtube');
		var obj = {};
		obj.id = -1;
		obj.title = eleTitle.getValue();
		obj.text = eleText.getValue();
		obj.youtube = eleYoutube.getValue();
		if(isEdit) {
			obj.id = parseInt(eleId.getValue(), 10);
		}
		return obj;
	}
});
