// These templates will be displayed as a dropdown in all media dialog.

var tinyMCETemplateList = [
	// Name, URL, Description
	["Simple snippet", "resources/templates/html/snippet1.htm", "Simple HTML snippet."],
	["Layout", "resources/templates/html/layout1.htm", "HTML Layout."],
	["Layout 2", "resources/templates/html/layout2.htm", "HTML Layout."]
];