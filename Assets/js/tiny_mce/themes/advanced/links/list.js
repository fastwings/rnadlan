// This list may be created by a server logic page PHP/ASP/ASPX/JSP in some backend system.
// There links will be displayed as a dropdown in all link dialogs if the "external_link_list_url"
// option is defined in TinyMCE init.

var tinyMCELinkList = new Array(
	// Name, URL
	["Tiny MCE", "http://tinymce.moxiecode.com"],
	["ExtJs", "http://www.sencha.com/products/extjs/"],
	["Rahul Singla", "http://www.rahulsingla.com"]
);
