﻿<?php

$op = $_REQUEST["op"];
$path = $_REQUEST["path"];
header("Content-Type: text/javascript");

//These are extra parameters you can pass to the server in each request by specifying extjsfilemanager_extraparams option in init.
$param1=$_REQUEST["param1"];
$param2=$_REQUEST["param2"];

$allowedExtensions = array("png", "gif", "jpg", "jpeg", "zip", "doc", "docx");

switch ($op)
{
	case "getFolders":
		getFolders($path);
		break;

	case "getFiles":
		getFiles($path);
		break;

	case "createFolder":
		createFolder($path);
		break;

	case "uploadFiles":
		uploadFiles($path);
		break;
}


function getFolders ($path)
{
	$path = validatePath($path);

	$l = array();
	if ($handle = opendir($path)) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != "..") {
				$fullPath = joinPaths($path, $file);
				if(is_dir($fullPath)) {
					$l[] = array (
							'text' => $file,
							'path' => $fullPath,
							'leaf' => FALSE,
							'singleClickExpand' => TRUE
						);
				}
			}
		}
		closedir($handle);
	}

	echo json_encode($l);
}

function getFiles ($path)
{
	$path = validatePath($path);

	$l = array();
	if ($handle = opendir($path)) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != "..") {
				$fullPath = joinPaths($path, $file);
				if(is_file($fullPath)) {
					$fullPath = str_replace('\\', '/', $fullPath);
					$l[] = array (
							'text' => $file,
							'virtualPath' => str_replace($_SERVER['DOCUMENT_ROOT'], '', $fullPath)
						);
				}
			}
		}
		closedir($handle);
	}

	echo json_encode($l);
}

function createFolder ($path)
{
	$path = validatePath($path);

	$name=$_REQUEST["name"];
	$dirPath = joinPaths($path, $name);
	mkdir($dirPath);
	
	echo "{}";
}

function uploadFiles ($path)
{
	global $allowedExtensions;
	
	header("Content-Type: text/html");
	$path = validatePath($path);

	$successFiles="";
	$errorFiles="";
	
	foreach($_FILES as $key => $file) {
		if ($file['size'] > 0) {
			if($file['error'] == 0) {
				$info = pathinfo($file['name']);
				$extension = $info['extension'];
				if(!in_array(strtolower($extension), $allowedExtensions)) {
					$errorFiles .= str_replace("Extension for {0} is not allowed.<br />", $file['name']);
				} else {
					$destination = joinPaths($path, $file['name']);
					if(!move_uploaded_file($file['tmp_name'], $destination)) {
						$errorFiles .= $file["name"] . ' upload failed.<br />';
					}
				}
			} else {
				$errorFiles .= $file["name"] . ' upload failed.<br />';
			}
		}
	}

	echo json_encode(array(
		'success' => TRUE,
		'successFiles' => $successFiles,
		'errorFiles' => $errorFiles
	));
}

function joinPaths() {
    $args = func_get_args();
    $paths = array();
    foreach ($args as $arg) {
        $paths = array_merge($paths, (array)$arg);
    }

    $paths = array_map(create_function('$p', 'return trim($p, DIRECTORY_SEPARATOR);'), $paths);
    $paths = array_filter($paths);
    return join(DIRECTORY_SEPARATOR, $paths);
}

function validatePath ($path)
{
	// The parent directory of this file is the application root for this test application.
	$basePath =joinPaths(dirname(__FILE__), 'Uploads');
	
	if(empty($path)) {
		$path = $basePath;
	}
	
	if(strpos($path, $basePath) !== 0 || strpos($path, '..') !== FALSE) {
		die('Invalid path.');
	}

	return ($path);
}
