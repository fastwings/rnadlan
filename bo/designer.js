/*
 * File: designer.js
 * Date: Sun Dec 04 2011 14:45:29 GMT+0200 (Jerusalem Standard Time)
 *
 * This file was generated by Ext Designer version 1.2.2.
 * http://www.sencha.com/products/designer/
 *
 * This file will be auto-generated each and everytime you export.
 *
 * Do NOT hand edit this file.
 */

Ext.Loader.setConfig({
    enabled: true
});

Ext.application({
    name: 'MyApp',

    stores: [
        'MyJsonStore1',
        'MyJsonStore2',
        'MyXmlStore2',
        'MyXmlTreeStore'
    ],

    launch: function() {
        Ext.QuickTips.init();

        var cmp1 = Ext.create('MyApp.view.MyWindow', {
            renderTo: Ext.getBody()
        });
        cmp1.show();
    }
});
