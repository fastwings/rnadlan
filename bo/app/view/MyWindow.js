/*
 * File: app/view/MyWindow.js
 * Date: Sun Dec 04 2011 14:51:45 GMT+0200 (Jerusalem Standard Time)
 *
 * This file was generated by Ext Designer version 1.2.2.
 * http://www.sencha.com/products/designer/
 *
 * This file will be generated the first time you export.
 *
 * You should implement event handling and custom methods in this
 * class.
 */

Ext.define('MyApp.view.MyWindow', {
    extend: 'MyApp.view.ui.MyWindow',

    initComponent: function() {
        var me = this;
        me.callParent(arguments);
    }
});