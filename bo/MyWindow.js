/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


Ext.define('MyApp.view.ui.MyWindow', {
    extend: 'Ext.window.Window',
    renderTo: document.body,
    draggable: false,
    height: 788,
    width: 1100,
    autoScroll: true,
    layout: {
        type: 'fit'
    },
    closable: false,
    title: 'Back Office',
    maximized: true,
    modal: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'tbtext',
                            text: 'Login By:'
                        },
                        {
                            xtype: 'button',
                            text: 'User Name',
                            menu: {
                                xtype: 'menu',
                                items: [
                                    {
                                        xtype: 'menuitem',
                                        text: 'User Profile'
                                    },
                                    {
                                        xtype: 'menuitem',
                                        text: 'Logout'
                                    }
                                ]
                            }
                        },
                        {
                            xtype: 'tbspacer'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'buttongroup',
                            width: 204,
                            title: 'Quick Controls',
                            columns: 10,
                            layout: {
                                columns: 10,
                                type: 'table'
                            },
                            items: [
                                {
                                    xtype: 'button',
                                    text: 'Pages List'
                                },
                                {
                                    xtype: 'button',
                                    text: 'News List'
                                },
                                {
                                    xtype: 'button',
                                    width: 78,
                                    text: 'Calender List'
                                }
                            ]
                        },
                        {
                            xtype: 'buttongroup',
                            width: 521,
                            title: 'Site Areas',
                            columns: 10,
                            layout: {
                                columns: 10,
                                type: 'table'
                            },
                            items: [
                                {
                                    xtype: 'button',
                                    text: 'Main Site',
                                    menu: {
                                        xtype: 'menu',
                                        items: [
                                            {
                                                xtype: 'menuitem',
                                                text: 'Site Settings'
                                            },
                                            {
                                                xtype: 'menuitem',
                                                text: 'Mail Settings'
                                            },
                                            {
                                                xtype: 'menuitem',
                                                text: 'Site Staff'
                                            },
                                            {
                                                xtype: 'menuitem',
                                                text: 'Site Headers'
                                            },
                                            {
                                                xtype: 'menuitem',
                                                text: 'Site Footer'
                                            }
                                        ]
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'Users',
                                    menu: {
                                        xtype: 'menu',
                                        items: [
                                            {
                                                xtype: 'menuitem',
                                                text: 'List'
                                            },
                                            {
                                                xtype: 'menuitem',
                                                text: 'New'
                                            }
                                        ]
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'Pages',
                                    menu: {
                                        xtype: 'menu',
                                        items: [
                                            {
                                                xtype: 'menuitem',
                                                text: 'List'
                                            },
                                            {
                                                xtype: 'menuitem',
                                                text: 'New'
                                            }
                                        ]
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'News',
                                    menu: {
                                        xtype: 'menu',
                                        items: [
                                            {
                                                xtype: 'menuitem',
                                                text: 'List'
                                            },
                                            {
                                                xtype: 'menuitem',
                                                text: 'New'
                                            }
                                        ]
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'Quick Tips',
                                    menu: {
                                        xtype: 'menu',
                                        items: [
                                            {
                                                xtype: 'menuitem',
                                                text: 'List'
                                            },
                                            {
                                                xtype: 'menuitem',
                                                text: 'New'
                                            }
                                        ]
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'Calender',
                                    menu: {
                                        xtype: 'menu',
                                        items: [
                                            {
                                                xtype: 'menuitem',
                                                text: 'Weekly Plan'
                                            },
                                            {
                                                xtype: 'menuitem',
                                                text: 'Monthly Plan'
                                            }
                                        ]
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'Races',
                                    menu: {
                                        xtype: 'menu',
                                        items: [
                                            {
                                                xtype: 'menuitem',
                                                text: 'Runners'
                                            },
                                            {
                                                xtype: 'menuitem',
                                                text: 'Biskals'
                                            }
                                        ]
                                    }
                                },
                                {
                                    xtype: 'button',
                                    width: 156,
                                    text: 'Contact Us'
                                }
                            ]
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'tbspacer'
                        },
                        {
                            xtype: 'button',
                            text: 'View Site'
                        },
                        {
                            xtype: 'tbspacer'
                        },
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            text: 'Logout'
                        }
                    ]
                },
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    layout: {
                        align: 'middle',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'tbtext',
                            tpl: Ext.create('Ext.XTemplate',''),
                            text: 'Site & Back office Build And Develop By Sivan Wolberg'
                        }
                    ]
                },
                {
                    xtype: 'treepanel',
                    height: 675,
                    width: 234,
                    title: 'Site Menu',
                    dock: 'left',
                    viewConfig: {

                    },
                    columns: [
                        {
                            xtype: 'treecolumn',
                            width: 198,
                            dataIndex: 'text',
                            flex: 1,
                            text: 'Controls'
                        },
                        {
                            xtype: 'gridcolumn',
                            width: 50,
                            align: 'center',
                            dataIndex: 'value',
                            text: '#'
                        }
                    ]
                }
            ],
            items: [
                {
                    xtype: 'tabpanel',
                    height: 700,
                    activeTab: 0,
                    items: [
                        {
                            xtype: 'panel',
                            height: 663,
                            autoScroll: true,
                            title: 'User List',
                            items: [
                                {
                                    xtype: 'gridpanel',
                                    title: 'Users Site Mangment',
                                    viewConfig: {

                                    },
                                    columns: [
                                        {
                                            xtype: 'gridcolumn',
                                            text: 'User Name'
                                        },
                                        {
                                            xtype: 'gridcolumn',
                                            text: 'Email'
                                        },
                                        {
                                            xtype: 'datecolumn',
                                            text: 'Last Login'
                                        },
                                        {
                                            xtype: 'datecolumn',
                                            text: 'Register Date'
                                        },
                                        {
                                            xtype: 'booleancolumn',
                                            text: 'Active'
                                        },
                                        {
                                            xtype: 'gridcolumn',
                                            text: 'User Role'
                                        },
                                        {
                                            xtype: 'actioncolumn',
                                            width: 246,
                                            items: [
                                                {

                                                },
                                                {

                                                }
                                            ]
                                        }
                                    ],
                                    selModel: Ext.create('Ext.selection.CheckboxModel', {

                                    })
                                }
                            ],
                            dockedItems: [
                                {
                                    xtype: 'pagingtoolbar',
                                    width: 360,
                                    displayInfo: true,
                                    dock: 'bottom'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            title: 'Tab 2'
                        },
                        {
                            xtype: 'panel',
                            title: 'Tab 3'
                        }
                    ],
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'top',
                            items: [
                                {
                                    xtype: 'buttongroup',
                                    title: 'Actions',
                                    columns: 3,
                                    layout: {
                                        columns: 3,
                                        type: 'table'
                                    },
                                    items: [
                                        {
                                            xtype: 'button',
                                            text: 'Add'
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Edit'
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Delete'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});

