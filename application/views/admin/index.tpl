<html>
    <head>
        <title>{$title}</title>
        <link rel="stylesheet" type="text/css"
        href="{$base_url_assets}/css/ext-all.css" />
        <script type="text/javascript" src="{$base_url_assets}/js/bootstrap.js"></script>
        <!--script src="http://extjs.cachefly.net/ext-4.0.2a/bootstrap.js" type="text/javascript"></script-->
        <script type="text/javascript" src="{$base_url_assets}/js/jquery.js"></script>
        <script type="text/javascript" src="{$base_url_assets}/js/support/defines.js"></script>
        <script type="text/javascript" src="{$base_url_assets}/js/support/menuSwitcher.js"></script>
        <script type="text/javascript" src="{$base_url_assets}/js/support/miscUtills.js"></script>
        <script type="text/javascript" src="{$base_url_assets}/js/tiny_mce/tiny_mce.js"></script>
        <script type="text/javascript" src="{$base_url_assets}/js/ux/editor_plugin.js"></script>
        <script type="text/javascript" src="{$base_url_assets}/js/ux/TinyMCE.js"></script>
    </head>
    <body>
        <script>
            var siteUrl = "{$siteRedirect}";
            var siteUrlUManger = "{$base_url}/BrowserHandler.php";

            var requestConfig = {};{foreach from=$AdminRequests key=k item=request}requestConfig.{$k} ='{$request}';{/foreach}
            Ext.onReady(function() {
                Ext.Loader.setConfig({
                    enabled : true,
                    enableDeadlockDetection : true,
                    paths : {
                        'Ext.ux' : '{$base_url_assets}/js/ux'

                    }
                });

                Ext.require(['Ext.data.*', 'Ext.grid.*', 'Ext.util.*', 'Ext.ux.data.PagingMemoryProxy', 'Ext.toolbar.Paging', 'Ext.ux.SlidingPager']);
                Ext.require(['MyApp.view.ui.Windows.*', 'MyApp.view.ui.Forms.*']);
                Ext.tip.QuickTipManager.init();
                Ext.application({
                    name : 'MyApp',
                    appFolder: '{$base_url_assets}/js/app',
                    stores : ['MyJsonStore1', 'MyJsonStore2', 'MyXmlStore2', 'MyXmlTreeStore'],
                    controllers : ['Users', 'Pages','News'],
                    launch : function() {
                        Ext.QuickTips.init();
                        var application = this;

                        var cmp1 = Ext.create('MyApp.view.MyWindow', {
                            renderTo : Ext.getBody()
                        });
                        cmp1.show();
                        var cmp2 = Ext.create('MyApp.view.MyPagingToolbar', {
                            renderTo : Ext.getBody()
                        });
                        cmp2.show();
                    }
                });

            });

        </script>
        <div id="ElementsEvent">
            &nbsp;
        </div>
    </body>
</html>