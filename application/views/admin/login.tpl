<html>
<head>
<title>{$title}</title>
<link rel="stylesheet" type="text/css"
	href="{$base_url_assets}/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="{$base_url_assets}/js/logindialog/flags.css" />
<link rel="stylesheet" type="text/css"
	href="{$base_url_assets}/js/logindialog/IconCombo.css" />
<link rel="stylesheet" type="text/css"
	href="{$base_url_assets}/js/logindialog/LoginDialog.css" />

<script type="text/javascript" src="{$base_url_assets}/js/bootstrap.js"></script>
<script type="text/javascript"
	src="{$base_url_assets}/js/logindialog/overrides.js"></script>
<script type="text/javascript"
	src="{$base_url_assets}/js/logindialog/IconCombo.js"></script>
<script type="text/javascript"
	src="{$base_url_assets}/js/logindialog/LoginDialog.js"></script>
</head>
<body>
<script>
var requestLogin = '{$loginRequest}';
var returnUrl = "{$loginPassedRedirect}";
var siteUrl = "{$siteRedirect}";
        Ext.onReady(function() {
            Ext.tip.QuickTipManager.init();

            var loginDialog = Ext.create('Ext.ux.LoginDialog', {
            	height:250,
            	languageField: {
            	    hidden: true
            	},
            	cancelReturnUrl: siteUrl,
                forgotPasswordLink: '<a href="http://support.microsoft.com/kb/189126" target="_blank">Forgot Password ?</a>',
                formPanel: {
                    url: requestLogin
                }
            });

            loginDialog.show();
            loginDialog.on('success',onSuccess);
            loginDialog.on('failure',onFailure);
        });
        function onSuccess() {
        	window.location = returnUrl;
        }
        function onFailure(frm,obj,msg) {
			//alert (msg);
        }
    </script>
</body>
</html>