<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>
	{block name=title}{/block}
</title>
<link rel="stylesheet" type="text/css" href="{$base_url_assets}/css/ext-all.css" />
<script type="text/javascript" src="{$base_url_assets}/js/bootstrap.js"></script>
<script type="text/javascript" src="{$base_url_assets}/js/jquery.js"></script>
</head>
<body>
{block name=body}{/block}
</body>
</html>