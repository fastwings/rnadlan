<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class AdminHandler extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this -> load -> model('user_model');
		$this -> output -> set_content_type('application/json');

		// Ideally you would autoload the parser
	}

	/**
	 *
	 * Do login into the system.
	 * dialog give this output:Array ( [data] => Array ( [User] => Array ( [username] => vcx [password] => cxvz [lang] => en-us ) ) )
	 */
	public function dologin() {
		$responses = new stdClass();
		$request = $_POST['data']['User'];
		if ($this -> user_model -> Login($request['username'], $request['password'])) {
			$responses -> success = true;
			$responses -> message = '';
		} else {
			$responses -> success = false;
			$responses -> message = "Password Or Username not found or incurrect";
		}
		$this -> output -> set_output(json_encode($responses));
	}

	public function dologout() {
		$this -> user_model -> DoLogout();
	}

	public function addPage() {
		$responses = new stdClass();
		$responses -> success = true;
		if ($this -> user_model -> IsLogin()) {
			$pageObj = new stdClass();
			$request = $_POST;
			$request['pageBody'] = htmlentities($request['pageBody']);
			$request['pageSubtitle'] = htmlentities($request['pageSubtitle']);
			$request['pageDesc'] = htmlentities($request['pageDesc']);
			$request['pageBody'] = htmlentities($request['pageBody']);
			$request['pageBody'] = htmlentities($request['pageBody']);
			foreach ($request as $key => $value) {
				$pageObj -> $key = $value;
			}
			$this -> page_model -> AddPage($pageObj);
			$this -> output -> set_output(json_encode($responses));
		} else {
			$responses -> success = false;
			$this -> output -> set_output(json_encode($responses));
		}
	}

	public function addNews() {
		$responses = new stdClass();
		$responses -> success = true;
		if ($this -> user_model -> IsLogin()) {
			$newsObj = new stdClass();
			$request = $_POST;
			$request['title'] = htmlentities($request['title']);
			$request['text'] = htmlentities($request['text']);
			$request['youtube'] = htmlentities($request['youtube']);
			foreach ($request as $key => $value) {
				$newsObj -> $key = $value;
			}
			$this -> news_model -> AddNews($newsObj);
			$this -> output -> set_output(json_encode($responses));
		} else {
			$responses -> success = false;
			$this -> output -> set_output(json_encode($responses));
		}
	}
}
?>