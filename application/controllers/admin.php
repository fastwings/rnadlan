<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Admin extends CI_Controller {
	private $_localReturnViewParams = array();
	public function __construct() {
		parent::__construct();
		$this -> load -> model('user_model');
		$this -> _localReturnViewParams = array('base_url' => base_url(), 'base_url_assets' => base_url() . "Assets");

		if (!$this -> user_model -> IsLogin() && current_url() != base_url('admin/login')) {
			redirect('admin/login', 'refresh');
		}
	}

	public function index() {
		$this -> _localReturnViewParams['title'] = "test Admin Site Page";
		$this -> _localReturnViewParams['base_url_assets_app'] = base_url() . "Assets/js/app";
		$this -> _localReturnViewParams['siteRedirect'] = base_url('site/index');
		$this -> _localReturnViewParams['AdminRequests'] = array();
		$this -> _localReturnViewParams['AdminRequests']['DoLogout'] = base_url('adminhandler/dologout');
		$this -> _localReturnViewParams['AdminRequests']['AddPage'] = base_url('adminhandler/addPage');
		$this -> _localReturnViewParams['AdminRequests']['AddNews'] = base_url('adminhandler/addNews');
		$this -> parser -> parse("admin/index", $this -> _localReturnViewParams);
	}

	public function login() {
		if ($this -> user_model -> IsLogin()) {
			redirect('admin/index', 'refresh');
		}
		$this -> _localReturnViewParams['title'] = "test Admin Login Page";
		$this -> _localReturnViewParams['loginRequest'] = base_url('adminhandler/dologin');
		$this -> _localReturnViewParams['loginPassedRedirect'] = base_url('admin/index');
		$this -> _localReturnViewParams['siteRedirect'] = base_url('site/index');
		$this -> parser -> parse("admin/login", $this -> _localReturnViewParams);
	}

}
?>