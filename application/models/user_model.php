<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class User_model extends CI_Model {
	
	function __construct() {
		// Call the Model constructor
    	parent::__construct();
	}
	
	function DoLogin($username, $password, $name, $email) {
		
		$sql = "INSERT INTO `rn_users` ( `username`, `password`, `name`, `email`, `status`) VALUES ( ?, ?, ?, ?, b'1')";
		$this->db->query ( $sql, array (3, 'live', 'Rick' ) );
	
	}
	function IsLogin() {
		if ($this->session->userdata ( 'isLogin' ))
			return true;
		return false;
	}
	function DoLogout() {
		$this->session->set_userdata ( 'isLogin', 0 );
	}
	function Login($username, $password) {
		$sql = "select count(*) as count from rn_users where username= ? and password= ? and status =1";
		$params = array ($username, $this->encrypt->sha1 ( $password ) );
		$query = $this->db->query ( $sql, $params );
		
		if ($query->num_rows () > 0) {
			$row = $query->row ();
			if (intval ( $row->count ) == 1) {
				$this->session->set_userdata ( 'isLogin', 1 );
				return true;
			}
		}
		return false;
	}

}
?>