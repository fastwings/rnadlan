<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Page_model extends CI_Model {

	function __construct() {
		// Call the Model constructor
		parent::__construct();
	}
	
	function IsPageExist() {
		if ($this->session->userdata ( 'isLogin' ))
			return true;
		return false;
	}

	function AddPage($pageObject) {
		$data = array(
			'meta_key' => $pageObject -> metaKey, 
			'meta_desc' => $pageObject -> metaDesc, 
			'title' => $pageObject -> metaTitle, 
			'subtitle' => $pageObject -> pageDesc, 
			'description' => $pageObject -> pageSubtitle, 
			'body' => $pageObject -> pageBody
			);

		$this -> db -> insert('pages', $data);
	}

}
?>