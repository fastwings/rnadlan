<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class News_model extends CI_Model {

	function __construct() {
		// Call the Model constructor
		parent::__construct();
	}

	function IsPageExist() {
		if ($this -> session -> userdata('isLogin'))
			return true;
		return false;
	}

	function AddNews($pageObject) {
		$data = array('title' => $pageObject -> title, 'body' => $pageObject -> text, 'youtube_link' => $pageObject -> youtube, );

		$this -> db -> insert('news', $data);
	}

}
?>