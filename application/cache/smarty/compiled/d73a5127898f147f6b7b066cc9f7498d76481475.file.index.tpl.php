<?php /* Smarty version Smarty-3.1.7, created on 2012-02-19 05:12:28
         compiled from "application/views\admin\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:253694f3e8c14bff214-90356595%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd73a5127898f147f6b7b066cc9f7498d76481475' => 
    array (
      0 => 'application/views\\admin\\index.tpl',
      1 => 1329624733,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '253694f3e8c14bff214-90356595',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_4f3e8c14c6ba0',
  'variables' => 
  array (
    'title' => 0,
    'base_url_assets' => 0,
    'siteRedirect' => 0,
    'base_url' => 0,
    'AdminRequests' => 0,
    'k' => 0,
    'request' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_4f3e8c14c6ba0')) {function content_4f3e8c14c6ba0($_smarty_tpl) {?><html>
    <head>
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
        <link rel="stylesheet" type="text/css"
        href="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/css/ext-all.css" />
        <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/bootstrap.js"></script>
        <!--script src="http://extjs.cachefly.net/ext-4.0.2a/bootstrap.js" type="text/javascript"></script-->
        <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/support/defines.js"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/support/menuSwitcher.js"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/support/miscUtills.js"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/tiny_mce/tiny_mce.js"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/ux/editor_plugin.js"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/ux/TinyMCE.js"></script>
    </head>
    <body>
        <script>
            var siteUrl = "<?php echo $_smarty_tpl->tpl_vars['siteRedirect']->value;?>
";
            var siteUrlUManger = "<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/BrowserHandler.php";

            var requestConfig = {};<?php  $_smarty_tpl->tpl_vars['request'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['request']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['AdminRequests']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['request']->key => $_smarty_tpl->tpl_vars['request']->value){
$_smarty_tpl->tpl_vars['request']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['request']->key;
?>requestConfig.<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
 ='<?php echo $_smarty_tpl->tpl_vars['request']->value;?>
';<?php } ?>
            Ext.onReady(function() {
                Ext.Loader.setConfig({
                    enabled : true,
                    enableDeadlockDetection : true,
                    paths : {
                        'Ext.ux' : '<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/ux'

                    }
                });

                Ext.require(['Ext.data.*', 'Ext.grid.*', 'Ext.util.*', 'Ext.ux.data.PagingMemoryProxy', 'Ext.toolbar.Paging', 'Ext.ux.SlidingPager']);
                Ext.require(['MyApp.view.ui.Windows.*', 'MyApp.view.ui.Forms.*']);
                Ext.tip.QuickTipManager.init();
                Ext.application({
                    name : 'MyApp',
                    appFolder: '<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/app',
                    stores : ['MyJsonStore1', 'MyJsonStore2', 'MyXmlStore2', 'MyXmlTreeStore'],
                    controllers : ['Users', 'Pages','News'],
                    launch : function() {
                        Ext.QuickTips.init();
                        var application = this;

                        var cmp1 = Ext.create('MyApp.view.MyWindow', {
                            renderTo : Ext.getBody()
                        });
                        cmp1.show();
                        var cmp2 = Ext.create('MyApp.view.MyPagingToolbar', {
                            renderTo : Ext.getBody()
                        });
                        cmp2.show();
                    }
                });

            });

        </script>
        <div id="ElementsEvent">
            &nbsp;
        </div>
    </body>
</html><?php }} ?>