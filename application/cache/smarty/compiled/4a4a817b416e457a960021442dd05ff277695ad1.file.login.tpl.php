<?php /* Smarty version Smarty-3.1.7, created on 2012-02-17 21:17:32
         compiled from "application/views\admin\login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:280244f3e9158c59ba5-08524866%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4a4a817b416e457a960021442dd05ff277695ad1' => 
    array (
      0 => 'application/views\\admin\\login.tpl',
      1 => 1329507681,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '280244f3e9158c59ba5-08524866',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_4f3e9158cb68b',
  'variables' => 
  array (
    'title' => 0,
    'base_url_assets' => 0,
    'loginRequest' => 0,
    'loginPassedRedirect' => 0,
    'siteRedirect' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_4f3e9158cb68b')) {function content_4f3e9158cb68b($_smarty_tpl) {?><html>
<head>
<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
<link rel="stylesheet" type="text/css"
	href="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/logindialog/flags.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/logindialog/IconCombo.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/logindialog/LoginDialog.css" />

<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/bootstrap.js"></script>
<script type="text/javascript"
	src="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/logindialog/overrides.js"></script>
<script type="text/javascript"
	src="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/logindialog/IconCombo.js"></script>
<script type="text/javascript"
	src="<?php echo $_smarty_tpl->tpl_vars['base_url_assets']->value;?>
/js/logindialog/LoginDialog.js"></script>
</head>
<body>
<script>
var requestLogin = '<?php echo $_smarty_tpl->tpl_vars['loginRequest']->value;?>
';
var returnUrl = "<?php echo $_smarty_tpl->tpl_vars['loginPassedRedirect']->value;?>
";
var siteUrl = "<?php echo $_smarty_tpl->tpl_vars['siteRedirect']->value;?>
";
        Ext.onReady(function() {
            Ext.tip.QuickTipManager.init();

            var loginDialog = Ext.create('Ext.ux.LoginDialog', {
            	height:250,
            	languageField: {
            	    hidden: true
            	},
            	cancelReturnUrl: siteUrl,
                forgotPasswordLink: '<a href="http://support.microsoft.com/kb/189126" target="_blank">Forgot Password ?</a>',
                formPanel: {
                    url: requestLogin
                }
            });

            loginDialog.show();
            loginDialog.on('success',onSuccess);
            loginDialog.on('failure',onFailure);
        });
        function onSuccess() {
        	window.location = returnUrl;
        }
        function onFailure(frm,obj,msg) {
			//alert (msg);
        }
    </script>
</body>
</html><?php }} ?>